#include "stdio.h"
#include "stdbool.h"

bool check(int a)               //функция проверки, введено ли число
{
	if (a > 0 && a < 120)
		return true;
	return false;
}

int main()
{
	printf("Enter your age: ");
	int age;
	scanf("%d", &age);
	if (!check(age))
	{
		printf("Something wrong with your age\n");
		return 0;
	}
	int days = age * 365 + age / 4;       //возможное кол-во лет - age/4
	printf("Your age in days: %d\n", days);
	return 0;
}
